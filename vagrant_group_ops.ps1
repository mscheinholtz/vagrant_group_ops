﻿[CmdletBinding()]
    Param(
        [Parameter(Mandatory=$False)]
        [ValidateSet('halt_all','destroy_all','start_all')]
        [string]$Command

    )

    BEGIN
    {
    }
    PROCESS
    {
        #Regex Pattern
        [regex]$Regex_Pattern = "^\w+"

        #Get all virtual boxes
        $All_Boxes = vagrant global-status | Select-String "virtualbox"

        #Get running virtual boxes
        $Running_Boxes = vagrant global-status | Select-String "virtualbox running"

        #Get all virtual boxes
        $Poweroff_Boxes = vagrant global-status | Select-String "virtualbox poweroff"

        #Output all virtual boxes
        Write-Output("All Virtual Boxes: " + $All_Boxes.Count)
        For($Iterator=0;$Iterator -lt $All_Boxes.Count; $Iterator++){
            Write-Output("`tBox " + ($Iterator + 1) + ": " + $Regex_Pattern.Match($All_Boxes[$Iterator]).Value)
        }

        #Output all running boxes
        Write-Output("Running Virtual Boxes: " + $Running_Boxes.Count)
        For($Iterator=0;$Iterator -lt $Running_Boxes.Count; $Iterator++){
            Write-Output("`tRunning Virtual Box " + ($Iterator + 1) + ": " + $Regex_Pattern.Match($Running_Boxes[$Iterator]).Value)
        }

        #Output all poweroff boxes
        Write-Output("Poweroff Virtual Boxes: " + $Poweroff_Boxes.Count)
        For($Iterator=0;$Iterator -lt $Poweroff_Boxes.Count; $Iterator++){
            Write-Output("`tPowerOff Virtual Box " + ($Iterator + 1) + ": " + $Regex_Pattern.Match($Poweroff_Boxes[$Iterator]).Value)
        }

        Switch($Command){
            "halt_all"{
                #Halt running boxes
                For($Iterator=0;$Iterator -lt $Running_Boxes.Count; $Iterator++){
                    Write-Output("Halting Running Virtual Box " + $Iterator + ": " + $Regex_Pattern.Match($Running_Boxes[$Iterator]).Value)
                    vagrant halt $Regex_Pattern.Match($Running_Boxes[$Iterator]).Value
                }

            }

            "destroy_all"{
                #Destroy all boxes
                For($Iterator=0;$Iterator -lt $All_Boxes.Count; $Iterator++){
                    Write-Output("Destroying Virtual Box " + $Iterator + ": " + $Regex_Pattern.Match($All_Boxes[$Iterator]).Value)
                    vagrant destroy $Regex_Pattern.Match($All_Boxes[$Iterator]).Value --force
                }
            }

            "start_all"{
                #Start all poweroff boxes
                For($Iterator=0;$Iterator -lt $Poweroff_Boxes.Count; $Iterator++){
                    Write-Output("Starting Poweroff Virtual Box " + $Iterator + ": " + $Regex_Pattern.Match($Poweroff_Boxes[$Iterator]).Value)
                    vagrant up $Regex_Pattern.Match($Poweroff_Boxes[$Iterator]).Value
                }
            }
        }
    }
    END
    {
    }