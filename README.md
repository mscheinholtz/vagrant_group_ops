# vagrant_group_ops

VAGRANT_GROUP_OPS
============

Python & PowerShell project to perform group/batch operations for Vagrant boxes


Usage
---------------------

**Python**

python vagrant_group_ops.py <operation>  
`python vagrant_group_ops.py halt_all`  
`python vagrant_group_ops.py destroy_all`  
`python vagrant_group_ops.py start_all`

**PowerShell**

powershell -file vagrant_group_ops.ps1 -command <operation>  
`powershell -file vagrant_group_ops.ps1 -command halt_all`  
`powershell -file vagrant_group_ops.ps1 -command destroy_all`  
`powershell -file vagrant_group_ops.ps1 -command start_all`  
