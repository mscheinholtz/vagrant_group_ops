"""Perform batch operations on vagrant virtual boxes"""
import subprocess
import io
import re
import sys

BOXES = subprocess.Popen(["vagrant", "global-status"], stdout=subprocess.PIPE)
ALL_BOXES = []
RUNNING_BOXES = []
POWEROFF_BOXES = []
ABORTED_BOXES = []

for line in io.TextIOWrapper(BOXES.stdout, encoding="utf-8"):
    if "virtualbox" in line:
        result = re.search("^\\w+", line)
        if "running" in line:
            if result:
                RUNNING_BOXES.append(result.group(0))
        elif "poweroff" in line:
            if result:
                POWEROFF_BOXES.append(result.group(0))
        elif "aborted" in line:
            if result:
                ABORTED_BOXES.append(result.group(0))
        if result:
            ALL_BOXES.append(result.group(0))

print("All Virtual Boxes: {}".format(ALL_BOXES))
print("Running Virtual Boxes: {}".format(RUNNING_BOXES))
print("Poweroff Virtual Boxes: {}".format(POWEROFF_BOXES))
print("Aborted Virtual Boxes: {}".format(ABORTED_BOXES))

if len(sys.argv) == 1:
    print("No group box operation argument entered")
elif len(sys.argv) == 2:
    if sys.argv[1] == "halt_all":
        for box_id in RUNNING_BOXES:
            print("Halting {}".format(box_id))
            HALT_ALL_COMMAND = subprocess.Popen(["vagrant", "halt", box_id], stdout=subprocess.PIPE).communicate()[0]
    elif sys.argv[1] == "destroy_all":
        for box_id in ALL_BOXES:
            print("Destroying {}".format(box_id))
            DESTROY_ALL_COMMAND = subprocess.Popen(["vagrant", "destroy", box_id, "--force"], stdout=subprocess.PIPE).communicate()[0]
    elif sys.argv[1] == "start_all":
        for box_id in POWEROFF_BOXES:
            print("Starting {}".format(box_id))
            START_ALL_COMMAND = subprocess.Popen(["vagrant", "up", box_id], stdout=subprocess.PIPE).communicate()[0]
else:
    print("Unrecognized argument {}".format(sys.argv[2]))
